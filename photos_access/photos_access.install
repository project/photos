<?php

/**
 * @file
 * Install, update, and uninstall functions for the Photos Access module.
 */

use Drupal\Core\Database\Database;

/**
 * Implements hook_schema().
 */
function photos_access_schema() {
  $schema['photos_access_album'] = [
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
      ],
      'nid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'viewid' => [
        'type' => 'int',
        'size' => 'tiny',
        'length' => 1,
        'default' => 0,
        'description' => '0: Open, 1: Locked, 2: User list, 3: Password',
      ],
      'pass' => [
        'type' => 'varchar',
        'length' => 128,
        'default' => '',
      ],
    ],
    'indexes' => [
      'nid' => ['nid'],
    ],
    'primary key' => ['id'],
  ];

  $schema['photos_access_user'] = [
    'fields' => [
      'id' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'uid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'collaborate' => [
        'type' => 'int',
        'size' => 'tiny',
        'length' => 1,
        'default' => 0,
        'not null' => TRUE,
      ],
    ],
    'indexes' => [
      'uid' => ['uid'],
    ],
    'primary key' => ['id', 'uid', 'collaborate'],
  ];

  return $schema;
}

/**
 * Implements hook_install().
 */
function photos_access_install() {
  \Drupal::messenger()->addMessage(t("Photos Access settings are available under admin/structure/photos Basic settings."));
}

/**
 * Implements hook_uninstall().
 */
function photos_access_uninstall() {
  // Update photos module settings.
  if (\Drupal::moduleHandler()->moduleExists('photos')) {
    \Drupal::configFactory()->getEditable('photos.settings')->set('photos_access_photos', 0)->save();
  }
}

/**
 * Implements hook_update_last_removed().
 */
function photos_access_update_last_removed() {
  return 8401;
}

/**
 * Add primary key to {photos_access_user}.
 */
function photos_access_update_10601() {
  $schema = Database::getConnection()->schema();

  try {
    // Update collaborate field to not null.
    $spec = [
      'type' => 'int',
      'size' => 'tiny',
      'length' => 1,
      'default' => 0,
      'not null' => TRUE,
    ];
    $schema->changeField('photos_access_user', 'collaborate', 'collaborate', $spec);
    // Drop any primary keys that have been assigned manually.
    $schema->dropPrimaryKey('photos_access_user');
    // Add new primary key.
    $schema->addPrimaryKey('photos_access_user', ['id', 'uid', 'collaborate']);
  }
  catch (\Exception $e) {
  }
}
